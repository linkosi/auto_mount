# auto mount
برای یافتن UUID مورد نظر خود دستور `blkid` را وارد کنید. سپس UUID مورد نظر خود را با UUID="11FF-3428" جایگزین کنید.

در مرحله بعد به جای uid=debian-transmission,gid=debian-transmission، یوزر مورد نظر خود را بگذارید.

سپس در مسیر `/etc/systemd/system/auto-mount.service`، محتویات زیر را قرار دهید:
```
[Unit]
Description=automatic mount USB External HDD
After=Network.target

[Service]
User=root
ExecStart=/home/ubuntu/auto-mount.sh --mount
ExecStop=/home/ubuntu/auto-mount.sh --umount

[Install]
WantedBy=multi-user.target
```

حالا دستور زیر را جهت راه‌اندازی این سرویس بعد از روشن شدن سیستم را وارد پایانه کنید:
`sudo systemctl enable auto-mount.service`

بعد از اتمام مراحل بالا، میتوانید با دستور‌های `sudo systemctl restart auto-mount`، `sudo systemctl stop auto-mount` و `sudo systemctl start auto-mount` اقدام به کنترل سرویس کنید.

* نکته:
به یاد داشته باشید که دستور `sudo systemctl stop auto-mount` جهت توقف سرویس و جدا کردن هارد اکسترنال از سیستم می‌باشد. پس موقعی که قصد جدا کردن هارد را دارید میتوانید این دستور را وارد کنید.
