#!/bin/bash

externalHDDUSB_UUID=`blkid | grep 'UUID="11FF-3428"' | awk {'print $1'}`
directory_Address=`echo ${externalHDDUSB_UUID} | sed -e "s/:$//g"`

function mount {
    while true; do
	sleep 5
	dir_SED=`echo ${directory_Address} | sed -e "s/\/dev\///g"`
	mount_Check=`lsblk | grep "$dir_SED" | awk {'print $7'}`

	if ! [[ $mount_Check == "/media" ]]; then
	    sudo mount $directory_Address /media -o uid=debian-transmission,gid=debian-transmission
	fi
    done
}

function umount {
    sudo umount $directory_Address
}

while [ -n "$1" ]; do
    case "$1" in
    --mount) mount ;;
    --umount) umount ;;
    *) echo "Option $1 not recognized" ;;
    esac
    shift
done
